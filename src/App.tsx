import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, useParams } from 'react-router-dom';
import './App.css';
import axios from 'axios';
import Header from './components/Header';
import Footer from './components/Footer';
import FlowerInfo from './components/FlowerInfo';
import FlowerCatalog from './components/FlowerCatalog';
import AdminFlowerInfo from './components/AdminFlowerInfo';
import AdminFlowerCatalog from './components/AdminFlowerCatalog';
import PropTypes from 'prop-types';

interface IPlant {
    id: number | undefined;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    plant_category_id: number;
    shade_tolerance_type_id: number;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IPlantProperty {
    id: number;
    title: string;
    description: string;
}

interface IPlantInfo {
    title: string;
    description: string;
    irrigationType: string;
    moistureType: string;
    plantCategory: string;
    shadeToleranceType: string;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IFlowerInfoOuterProps {
    plants: Array<IPlant>;
}

interface IAdminEditFlowerInfoProps {
    deleteFlower: (id: number | undefined | null) => () => void;
    updateFlower: () => (plant: IPlant) => void;
    addFlower: () => (plant: IPlant) => void;
}

function App() {
    const [plants, setPlants] = useState<Array<IPlant>>([]);
    const [moistureTypes, setMoistureTypes] = useState<Array<IPlantProperty>>([]);
    const [plantCategories, setPlantCategories] = useState<Array<IPlantProperty>>([]);
    const [shadeToleranceTypes, setShadeToleranceTypes] = useState<Array<IPlantProperty>>([]);
    const [irrigationTypes, setIrrigationTypes] = useState<Array<IPlantProperty>>([]);

    useEffect(() => {
        axios
            .get('http://localhost:3000/plants')
            .then((res) => {
                setPlants(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/irrigation-types')
            .then((res) => {
                console.log(res.data);
                setIrrigationTypes(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/moisture-types')
            .then((res) => {
                setMoistureTypes(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/plant-categories')
            .then((res) => {
                setPlantCategories(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/shade-tolerance-types')
            .then((res) => {
                setShadeToleranceTypes(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    function addFlower() {
        return (val: IPlant) => {
            setPlants([...plants, val]);
            console.log('Добавили еще данных!');
        };
    }

    function updateFlower() {
        return (val: IPlant) => {
            console.log('Обновили!');
            setPlants(
                plants.map((plant) => {
                    if (plant.id === parseInt(String(val.id))) {
                        return val;
                    }
                    return plant;
                }),
            );
        };
    }

    const deleteFlower = (id: number | undefined | null) => () => {
        if (!id) return;
        axios
            .delete(`http://localhost:3000/admin/plants/delete/${id}`)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
        setPlants(plants.filter((plant) => plant.id !== id));
    };

    const getPlantInfo = (id: number | undefined | null): IPlantInfo | null => {
        if (!id) return null;
        const plant = plants.find((plant) => plant.id === id);
        if (!plant) return null;
        const moistureType = moistureTypes.find((mt) => mt.id === plant.moisture_type_id);
        const plantCategory = plantCategories.find((pc) => pc.id === plant.plant_category_id);
        const shadeToleranceType = shadeToleranceTypes.find((stt) => stt.id === plant.shade_tolerance_type_id);
        const irrigationType = irrigationTypes.find((it) => it.id === plant.irrigation_type_id);
        if (!moistureType || !plantCategory || !shadeToleranceType || !irrigationType) {
            return null;
        }
        const moistureTypeTitle = moistureType.title;
        const plantCategoryTitle = plantCategory.title;
        const shadeToleranceTypeTitle = shadeToleranceType.title;
        const irrigationTypeTitle = irrigationType.title;

        const plantInfo: IPlantInfo = {
            title: plant.title,
            description: plant.description,
            moistureType: moistureTypeTitle,
            plantCategory: plantCategoryTitle,
            shadeToleranceType: shadeToleranceTypeTitle,
            irrigationType: irrigationTypeTitle,
            image1: plant.image1,
            image2: plant.image2,
            image3: plant.image3,
        };
        return plantInfo;
    };

    function FlowerInfoOuter({ plants }: IFlowerInfoOuterProps) {
        const { id } = useParams<{ id: string }>();
        const plant = getPlantInfo(parseInt(id));
        return <FlowerInfo plant={plant} />;
    }

    const AdminAddFlowerInfo = (props: IAdminEditFlowerInfoProps): React.ReactElement => {
        const { deleteFlower, updateFlower, addFlower } = props;
        return <AdminFlowerInfo deleteFlower={deleteFlower} updateFlower={updateFlower()} addFlower={addFlower()} />;
    };

    AdminAddFlowerInfo.propTypes = {
        deleteFlower: PropTypes.func,
        updateFlower: PropTypes.func,
        addFlower: PropTypes.func,
    };

    const AdminEditFlowerInfo = (props: IAdminEditFlowerInfoProps): React.ReactElement => {
        const { id } = useParams<{ id: string }>();
        const { deleteFlower, updateFlower, addFlower } = props;
        return (
            <AdminFlowerInfo
                id={parseInt(id)}
                deleteFlower={deleteFlower}
                updateFlower={updateFlower()}
                addFlower={addFlower()}
            />
        );
    };

    AdminEditFlowerInfo.propTypes = {
        deleteFlower: PropTypes.func,
        updateFlower: PropTypes.func,
        addFlower: PropTypes.func,
    };

    return (
        <div className="page">
            <Header />
            <div className="page-main">
                <Router>
                    <Switch>
                        <Route path="/admin/plants/edit/:id">
                            <AdminEditFlowerInfo
                                deleteFlower={deleteFlower}
                                updateFlower={updateFlower}
                                addFlower={addFlower}
                            />
                        </Route>
                        <Route path="/admin/plants/add">
                            <AdminAddFlowerInfo
                                deleteFlower={deleteFlower}
                                addFlower={addFlower}
                                updateFlower={updateFlower}
                            />
                        </Route>
                        <Route path="/admin/plants">
                            <AdminFlowerCatalog plants={plants} />
                        </Route>
                        <Route path="/plants/:id">
                            <FlowerInfoOuter plants={plants} />
                        </Route>
                        <Route path="/">
                            <FlowerCatalog plants={plants} />
                        </Route>
                    </Switch>
                </Router>
            </div>
            <Footer />
        </div>
    );
}

export default App;
