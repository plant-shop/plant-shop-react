import React, { MouseEventHandler, ReactElement, useEffect, useState } from 'react';
import axios from 'axios';
import { Field, Form, withFormik, FormikProps, FormikHelpers } from 'formik';
import '../styles/main.scss';
import { Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Thumb from './Thumb';
import PropTypes from 'prop-types';

interface IPlant {
    id: number | undefined;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    plant_category_id: number;
    shade_tolerance_type_id: number;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IPlantPropertyOptions {
    id: number;
    title: string;
    description: string;
}

interface IFormFieldProps {
    name: string;
    id: string;
    label: string;
    placeholder?: string;
    required?: boolean;
}

interface IFlowerInfoProps {
    id?: number;
    addFlower: (plant: IPlant) => void;
    updateFlower: (plant: IPlant) => void;
    deleteFlower: (id: number | undefined) => () => void;
}

interface FormValues {
    [index: string]: number | Blob | undefined | string | null;
    id: number | undefined;
    image1: Blob | null;
    image2: Blob | null;
    image3: Blob | null;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    shade_tolerance_type_id: number;
    plant_category_id: number;
    // startTemperature: number;
    // endTemperature: number;
}

interface OtherFormProps {
    id: number | undefined;
    irrigationTypes: Array<IPlantPropertyOptions>;
    moistureTypes: Array<IPlantPropertyOptions>;
    shadeToleranceTypes: Array<IPlantPropertyOptions>;
    plantCategories: Array<IPlantPropertyOptions>;
    addFlower: (flower: IPlant) => void;
    updateFlower: (flower: IPlant) => void;
    deleteFlower: (id: number | undefined) => () => void;
}

interface MyFormProps {
    id?: number;
    initialImage1?: null | string;
    initialImage2?: null | string;
    initialImage3?: null | string;
    initialTitle?: string;
    initialDescription?: string;
    initialIrrigationType?: number;
    initialMoistureType?: number;
    initialShadeToleranceType?: number;
    initialPlantCategory?: number;
    initialStartTemperature?: number;
    initialEndTemperature?: number;
}

const renderFormFieldOptions = (options: Array<IPlantPropertyOptions>) =>
    options.map((opt) => (
        <option key={opt.id} value={opt.id}>
            {opt.title}
        </option>
    ));

const renderFormFieldSelect = (options: Array<IPlantPropertyOptions>, props: IFormFieldProps) => {
    const { id, label, name, required } = props;
    return (
        <div className="form-group">
            <label htmlFor={`${id}`}>{label}</label>
            <Field className="form-control" as="select" id={id} name={name} required={required}>
                {renderFormFieldOptions(options)}
            </Field>
        </div>
    );
};

renderFormFieldSelect.propTypes = {
    id: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string,
    required: PropTypes.bool,
};

const renderFormFieldInput = (props: IFormFieldProps) => {
    const { id, label, name, placeholder, required } = props;
    return (
        <div className="form-group">
            <label htmlFor={id}>{label}</label>
            <Field className="form-control" id={id} name={name} placeholder={placeholder} required={required} />
        </div>
    );
};

const renderFormFieldTextarea = (props: IFormFieldProps) => {
    const { id, label, name, placeholder } = props;
    return (
        <div className="form-group">
            <label htmlFor={`${id}`}>{label}</label>
            <Field as="textarea" className="form-control" id={id} name={name} placeholder={placeholder} />
        </div>
    );
};

const renderPlantTitleInput = () =>
    renderFormFieldInput({
        id: 'flower-title',
        name: 'title',
        label: 'Название цветка',
        placeholder: 'Flower title',
        required: true,
    });

const renderPlantDescriptionInput = () =>
    renderFormFieldTextarea({
        id: 'flower-descr',
        name: 'description',
        label: 'Описание цветка',
        required: true,
    });

const renderIrrigationTypeSelect = (irrigationTypes: Array<IPlantPropertyOptions>) =>
    renderFormFieldSelect(irrigationTypes, {
        id: 'flower-watering',
        name: 'irrigation_type_id',
        label: 'Полив',
        required: true,
    });

const renderShadeToleranceTypeSelect = (shadeToleranceTypes: Array<IPlantPropertyOptions>) =>
    renderFormFieldSelect(shadeToleranceTypes, {
        id: 'flower-light',
        name: 'shade_tolerance_type_id',
        label: 'Солнце',
        required: true,
    });

const renderMoistureTypeSelect = (moistureTypes: Array<IPlantPropertyOptions>) =>
    renderFormFieldSelect(moistureTypes, {
        id: 'flower-wet',
        name: 'moisture_type_id',
        label: 'Влажность',
        required: true,
    });

const renderPlantCategorySelect = (plantCategories: Array<IPlantPropertyOptions>) =>
    renderFormFieldSelect(plantCategories, {
        id: 'flower-category',
        name: 'plant_Category_id',
        label: 'Тип',
        required: true,
    });

const renderPlantImageInput = (
    props: IFormFieldProps,
    setFieldValue: (name: string, value: Blob | undefined) => void,
    values: FormValues,
) => {
    const { id, label, name, required } = props;
    return (
        <>
            {/*{values[name] !== '' && typeof values[name] === 'string' && (*/}
            {/*    <img src={values[name]} id={id} className="img-thumbnail mt-2" height={200} width={200} alt="..." />*/}
            {/*)}*/}
            {/*{values[name] !== '' && typeof values[name] !== 'string' && <Thumb file={values[name]} />}*/}
            <div className="file-input">
                <label className="file-input__label" data-img={id}>
                    {label}
                    {/* <Field type="file" name={name} className="file-input__input" required={required}/> */}
                    <input
                        id={name}
                        name={name}
                        type="file"
                        required={required}
                        onChange={(event) => {
                            if (event && event.currentTarget && event.currentTarget.files) {
                                setFieldValue(name, event?.currentTarget?.files[0]);
                            }
                        }}
                        className="file-input__input"
                    />
                </label>
            </div>
        </>
    );
};

const renderSubmitBtn = (btnText: string) => <input type="submit" className="btn btn-primary" value={btnText} />;

const renderDeleteBtn = (id: number | undefined, onClickHandler: () => void): ReactElement | null => {
    if (!id) return null;
    return (
        <Link to="/admin/plants" className="btn btn-danger" onClick={onClickHandler}>
            Удолить
        </Link>
    );
};

const InnerForm = (props: OtherFormProps & FormikProps<FormValues>) => {
    const {
        irrigationTypes,
        moistureTypes,
        shadeToleranceTypes,
        plantCategories,
        id,
        setFieldValue,
        values,
        addFlower,
        updateFlower,
        deleteFlower,
    } = props;
    const { isSubmitting } = props;
    return (
        <Form>
            <Col sm={12}>
                <Link to="/admin/plants">К списку</Link>
            </Col>
            <Col sm={12} lg={6} xl={6}>
                <Row className="align-content-center justify-content-center">
                    <Col sm={4}>
                        {renderPlantImageInput(
                            { id: 'flower-img-1', name: 'image1', label: 'Choose color...' },
                            setFieldValue,
                            values,
                        )}
                    </Col>
                    <Col sm={4}>
                        {renderPlantImageInput(
                            { id: 'flower-img-2', name: 'image2', label: 'Choose color...' },
                            setFieldValue,
                            values,
                        )}
                    </Col>
                    <Col sm={4}>
                        {renderPlantImageInput(
                            { id: 'flower-img-3', name: 'image3', label: 'Choose color...' },
                            setFieldValue,
                            values,
                        )}
                    </Col>
                </Row>
            </Col>
            <Col sm={12} lg={6} xl={6}>
                {renderPlantTitleInput()}
                {renderShadeToleranceTypeSelect(shadeToleranceTypes)}
                {renderIrrigationTypeSelect(irrigationTypes)}
                {renderMoistureTypeSelect(moistureTypes)}
                {renderPlantCategorySelect(plantCategories)}
                {renderPlantDescriptionInput()}
                {id ? renderSubmitBtn('Обновить') : renderSubmitBtn('Добавить')}
                {renderDeleteBtn(id, deleteFlower(id))}
            </Col>
        </Form>
    );
};

const MyForm = withFormik<MyFormProps & OtherFormProps, FormValues>({
    handleSubmit(values: FormValues, opt): void {
        const data = new FormData();

        for (const key in values) {
            let value = values[key];
            if (value === undefined || value === null) continue;
            if (typeof value === 'number') {
                value = String(value);
            }
            data.append(key, value);
        }

        const options = {
            headers: { 'Content-type': 'application/json' },
        };
        const url = values.id ? `http://localhost:3000/plants/${values.id}` : 'http://localhost:3000/plants';
        const handler = values.id ? opt.props.updateFlower : opt.props.addFlower;
        axios
            .post<IPlant>(url, data, options)
            .then((res) => {
                const updatePlant: IPlant = {
                    id: parseInt(String(res.data.id)),
                    description: res.data.description,
                    title: res.data.title,
                    moisture_type_id: parseInt(String(res.data.moisture_type_id)),
                    irrigation_type_id: parseInt(String(res.data.irrigation_type_id)),
                    shade_tolerance_type_id: parseInt(String(res.data.shade_tolerance_type_id)),
                    plant_category_id: parseInt(String(res.data.plant_category_id)),
                    image1: res.data.image1 ? res.data.image1 : null,
                    image2: res.data.image2 ? res.data.image2 : null,
                    image3: res.data.image3 ? res.data.image3 : null,
                };
                handler(updatePlant);
            })
            .catch((err) => {
                console.log(err);
            });
    },

    mapPropsToValues: (props) => ({
        id: props.id,
        image1: null,
        image2: null,
        image3: null,
        title: props.initialTitle || '',
        description: props.initialDescription || '',
        irrigation_type_id: props.initialIrrigationType || 1,
        moisture_type_id: props.initialMoistureType || 4,
        shade_tolerance_type_id: props.initialShadeToleranceType || 1,
        plant_category_id: props.initialPlantCategory || 1,
    }),
})(InnerForm);

const FlowerInfo = ({ id, addFlower, updateFlower, deleteFlower }: IFlowerInfoProps): ReactElement => {
    const [initialValues, setInitialValues] = useState<MyFormProps>();
    const [moistureTypes, setMoistureTypes] = useState([]);
    const [plantCategories, setPlantCategories] = useState([]);
    const [shadeToleranceTypes, setShadeToleranceTypes] = useState([]);
    const [irrigationTypes, setIrrigationTypes] = useState([]);

    useEffect(() => {
        if (!id) {
            setInitialValues({});
        }
        axios
            .get<IPlant>(`http://localhost:3000/plants/${id ? id : ''}`)
            .then((res) => {
                const data: IPlant = res.data;
                if (!data) return;
                const newInitialValues: MyFormProps = {
                    initialImage1: null,
                    initialImage2: null,
                    initialImage3: null,
                    initialTitle: data.title || '',
                    initialDescription: data.description,
                    initialIrrigationType: data.irrigation_type_id,
                    initialMoistureType: data.moisture_type_id,
                    initialShadeToleranceType: data.shade_tolerance_type_id,
                    initialPlantCategory: data.plant_category_id,
                };
                setInitialValues(newInitialValues);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/irrigation-types')
            .then((res) => {
                setIrrigationTypes(res.data);
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/moisture-types')
            .then((res) => {
                setMoistureTypes(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/plant-categories')
            .then((res) => {
                setPlantCategories(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    useEffect(() => {
        axios
            .get('http://localhost:3000/shade-tolerance-types')
            .then((res) => {
                setShadeToleranceTypes(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <Container>
            <Row className="justify-content-center">
                {initialValues && (
                    <MyForm
                        irrigationTypes={irrigationTypes}
                        moistureTypes={moistureTypes}
                        plantCategories={plantCategories}
                        shadeToleranceTypes={shadeToleranceTypes}
                        id={id}
                        addFlower={addFlower}
                        updateFlower={updateFlower}
                        deleteFlower={deleteFlower}
                        {...initialValues}
                    />
                )}
            </Row>
        </Container>
    );
};

export default FlowerInfo;
