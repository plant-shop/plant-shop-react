import React from 'react';
import '../styles/main.scss';
import { Container, Row, Col } from 'react-bootstrap';

function Footer() {
    return (
        <footer className="page-footer page__footer">
            <Container>
                <Row className="justify-content-center justify-content-sm-between">
                    <Col sm={6} md={3}>
                        <div className="copyright">All rights reserved ©</div>
                    </Col>
                    <Col sm={6} md={3}>
                        <div className="author">
                            <span>Author:</span>
                            <span className="author__name">Vishnik Taras</span>
                        </div>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
}

export default Footer;
