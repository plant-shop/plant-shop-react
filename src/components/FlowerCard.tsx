import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/main.scss';
import { Card, Button } from 'react-bootstrap';

interface IPlant {
    id: number | undefined;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    plant_category_id: number;
    shade_tolerance_type_id: number;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IFlowerCardProps {
    plant: IPlant;
}

function orderBtnClickHandler(id: number) {
    return () => {
        console.log(`Заказ товара ${id}`);
    };
}

const FlowerCard = ({ plant }: IFlowerCardProps) => {
    const { id, title, description, image1 } = plant;
    if (!id) {
        return null;
    }
    return (
        <Card>
            <Card.Body>
                <Link to={`/plants/${id}`} className="card-img-top">
                    <img src={image1 ? image1 : undefined} className="card-img-top" alt="..." />
                </Link>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{description}</Card.Text>
                <Button onClick={orderBtnClickHandler(id)}>Заказать</Button>
                <Link to={`/plants/${id}`} className="btn btn-primary">
                    Подробнее
                </Link>
            </Card.Body>
        </Card>
    );
};

export default FlowerCard;
