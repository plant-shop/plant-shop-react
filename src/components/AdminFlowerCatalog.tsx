import React, { useEffect, useState } from 'react';
import '../styles/main.scss';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { Link } from 'react-router-dom';
import AdminFlowerCard from './AdminFlowerCard';

interface IPlant {
    id: number | undefined;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    plant_category_id: number;
    shade_tolerance_type_id: number;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IAdminFlowerCatalogProps {
    plants: Array<IPlant>;
}

function renderFlowerCards(plants: Array<IPlant>) {
    return plants.map((plant, index) => {
        if (!plant.id) return null;
        return (
            <Col sm={12} md={6} lg={6} xl={4} key={plant.id}>
                <AdminFlowerCard id={plant.id} title={plant.title} text={plant.description} img={plant.image1} />
            </Col>
        );
    });
}

function AdminFlowerCatalog({ plants }: IAdminFlowerCatalogProps) {
    return (
        <Container>
            <Row>
                <Col sm={12}>
                    <h2>Каталог</h2>
                    <Link to="/admin/plants/add" className="btn btn-primary">
                        Добавить
                    </Link>
                </Col>
            </Row>
            <Row className="flower-list">{renderFlowerCards(plants)}</Row>
        </Container>
    );
}

export default AdminFlowerCatalog;
