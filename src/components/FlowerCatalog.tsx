import React from 'react';
import '../styles/main.scss';
import { Container, Row, Col } from 'react-bootstrap';
import FlowerCard from './FlowerCard';

interface IPlant {
    id: number | undefined;
    title: string;
    description: string;
    irrigation_type_id: number;
    moisture_type_id: number;
    plant_category_id: number;
    shade_tolerance_type_id: number;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

function renderFlowerCards(plants: Array<IPlant>) {
    return plants.map((plant) => (
        <Col sm={12} md={6} lg={6} xl={4} key={plant.id}>
            <FlowerCard plant={plant} />
        </Col>
    ));
}

interface IFlowerCatalogProps {
    plants: Array<IPlant>;
}

function FlowerCatalog({ plants }: IFlowerCatalogProps): React.ReactElement {
    return (
        <Container>
            <Row>
                <Col sm={12}>
                    <h2>Каталог</h2>
                </Col>
            </Row>
            <Row className="flower-list">{renderFlowerCards(plants)}</Row>
        </Container>
    );
}

export default FlowerCatalog;
