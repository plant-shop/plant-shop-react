import React, { ReactElement } from 'react';
import '../styles/main.scss';
import { Row, Col, Container, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

interface IPlantInfo {
    title: string;
    description: string;
    irrigationType: string;
    moistureType: string;
    plantCategory: string;
    shadeToleranceType: string;
    image1: string | null;
    image2: string | null;
    image3: string | null;
}

interface IFlowerInfoProps {
    plant: IPlantInfo | null;
}

const renderCarouselItems = (images: Array<string | null>): Array<ReactElement | null> =>
    images.map((img: string | null, index) => {
        if (!img) return null;
        return (
            <Carousel.Item key={index}>
                <img className="d-block w-100" src={img} alt="First slide" />
            </Carousel.Item>
        );
    });

const renderCarousel = (images: Array<string | null>): React.ReactElement => (
    <Carousel>{renderCarouselItems(images)}</Carousel>
);

const FlowerInfo = ({ plant }: IFlowerInfoProps): ReactElement => {
    return (
        <Container>
            {plant && (
                <Row className="justify-content-center">
                    <Col sm={12} lg={6} xl={6}>
                        {renderCarousel([plant.image1, plant.image2, plant.image3])}
                    </Col>
                    <Col sm={12} lg={6} xl={6}>
                        <h3 className="flower-info__title">{plant.title}</h3>
                        <p className="flower-info__descr">{plant.description}</p>
                        <ul className="list-group care-req">
                            <li className="list-group-item care-req__item">
                                <span className="care-req__type">Полив</span>:
                                <span className="care-req__value">{plant.irrigationType}</span>
                            </li>
                            <li className="list-group-item care-req__item">
                                <span className="care-req__type">Солнце</span>:
                                <span className="care-req__value">{plant.shadeToleranceType}</span>
                            </li>
                            <li className="list-group-item care-req__item">
                                <span className="care-req__type">Температура</span>:
                                <span className="care-req__value">
                                    {25}-{30} Градусов
                                </span>
                            </li>
                            <li
                                className="list-group-item care-req__item"
                                data-toggle="tooltip"
                                data-html="false"
                                data-placement="left"
                                title="30-60%. Идеально - 45%"
                            >
                                <span className="care-req__type">Влажность</span>:
                                <span className="care-req__value">{plant.moistureType}</span>
                            </li>
                            <li className="list-group-item care-req__item">
                                <span className="care-req__type">Категория</span>:
                                <span className="care-req__value">{plant.plantCategory}</span>
                            </li>
                        </ul>
                        <div className="flower-info__btns-container">
                            <button
                                type="button"
                                className="btn btn-primary btn-lg"
                                data-toggle="modal"
                                data-target="#exampleModal"
                                data-whatever="@mdo"
                            >
                                Заказать
                            </button>
                            <Link to="/plants" className="btn btn-primary btn-lg">
                                В главное меню
                            </Link>
                        </div>
                    </Col>
                </Row>
            )}
        </Container>
    );
};

export default FlowerInfo;
