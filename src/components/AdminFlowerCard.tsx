import React, { FC } from 'react';
import '../styles/main.scss';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

interface IFlowerCardProps {
    id: number | undefined;
    img: string | null;
    title: string;
    text: string;
}

const AdminFlowerCard = ({ img, title, text, id }: IFlowerCardProps): React.ReactElement => (
    <Card>
        <Card.Body>
            <a href="flower.html" className="card-img-top">
                <img src={img ? img : undefined} className="card-img-top" alt="..." />
            </a>
            <Card.Title>{title}</Card.Title>
            <Card.Text>{text}</Card.Text>
            <Link to={`/admin/plants/edit/${id ? id : ''}`} className="btn btn-primary">
                Подробнее
            </Link>
        </Card.Body>
    </Card>
);
export default AdminFlowerCard;
