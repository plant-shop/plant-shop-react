import React, { ReactElement } from 'react';
import '../styles/main.scss';
import { Navbar, Nav, Container } from 'react-bootstrap';

const Header = (): ReactElement => (
    <>
        <Navbar collapseOnSelect expand="lg" fixed="top" bg="light" variant="light">
            <Container>
                <Navbar.Brand href="#home" className="logo">
                    <img className="logo__img" src="../images/logo.png" alt="#" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav fill className="mr-auto">
                        <Nav.Link href="#features">Каталог</Nav.Link>
                        <Nav.Link href="#pricing">О нас</Nav.Link>
                        <Nav.Link href="#features">Свяжитесь с нами</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </>
);

export default Header;
