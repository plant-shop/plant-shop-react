import React from 'react';

interface IThumbProps {
    file: File;
    loading: boolean;
}

interface IThumbState {
    thumb: any;
    loading: any;
}

export default class Thumb extends React.Component<{ file: Blob }, IThumbState> {
    state = {
        loading: false,
        thumb: undefined,
    };

    UNSAFE_componentWillReceiveProps(nextProps: IThumbProps) {
        if (!nextProps.file) {
            return;
        }

        this.setState({ loading: true }, () => {
            const reader = new FileReader();

            reader.onloadend = () => {
                this.setState({ loading: false, thumb: reader.result });
            };

            reader.readAsDataURL(nextProps.file);
        });
    }

    render() {
        const { file } = this.props;
        const { loading, thumb } = this.state;

        if (!file) {
            return null;
        }

        if (loading) {
            return <p>loading...</p>;
        }

        return <img src={thumb} alt="file" className="img-thumbnail mt-2" height={200} width={200} />;
    }
}
